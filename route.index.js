var express = require("express");
var router = express.Router();
var fs = require('fs').promises;

router.get("/get-list", getCardList);

router.post("/add-card", saveCard);

router.get('/delete-card', deleteCard);

module.exports = router;

async function getCardList(req, res) {
    try {
        let data = await fs.readFile('./wallet-list.json', 'utf8');
        data = checkData(data);
        res.status(200).send({ code: 'success', data: data ?? [] });
    }
    catch (err) {
        res.status(404).send({ code: 'failure', error: err });
    }

}

function checkData(data) {
    let myObj = [];
    if (data) {
        myObj = JSON.parse(data);
    }
    return myObj;
}

async function saveCard(req, res) {
    try {
        let result = await fs.readFile('./wallet-list.json', 'utf8');
        result = checkData(result);
        let list = [];
        if (result && Array.isArray(result)) {
            list = [...result, req.body];
        } else {
            list.push(req.body);
        }
        await fs.writeFile('./wallet-list.json', JSON.stringify(list, null, 2));
        res.status(200).send({ code: 'success', data: req.body });
    } catch (err) {
        res.status(404).send({ code: 'failure', error: err });
    }
}

async function deleteCard(req, res) {
    try {
        let result = await fs.readFile('./wallet-list.json', 'utf8');
        result = checkData(result);
        let list = result.filter((ele) => ele.cardNo != req.query.cardNo);
        await fs.writeFile('./wallet-list.json', JSON.stringify(list, null, 2));
        res.status(200).send({ code: 'success', data: null });
    } catch (err) {
        res.status(404).send({ code: 'failure', error: err });
    }
}


