var express = require('express');
var app = express();
var http = require('http').createServer(app);
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;

app.all('/*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PATCH', 'DELETE');
    res.header("Access-Control-Allow-Headers", 'Origin, Content-Type, Accept');
    res.header('Access-Control-Expose-Headers', 'Origin, Content-Type, Accept');
    res.header('Access-Control-Allow-Credentials', 'true');

    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        return next();
    }
});

app.use(bodyParser.json({ limit: "50mb" }));
app.use(require("./route.index"));

http.listen(port, (err) => {
    err ? console.log("server failed or net issue") : console.log("server started");
})